variable "Gitlab_Token" {}
variable "ssh-key" {}

provider "gitlab" {
    token = var.Gitlab_Token
}

resource "gitlab_project" "example-terraform" {
    name = "example-terraform"
    description = "Terraform example for gitlab"

    visibility_level = "public"
}

resource "gitlab_deploy_key" "example-terraform" {
  project = "jon4h/example-terraform"
  title   = "HP Laptop"
  key     = var.ssh-key
  can_push = true
}
